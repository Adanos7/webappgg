﻿using System;

namespace WebApplicationGG.Models
{
    public class Message
    {
        public string clientUniqueId { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public DateTime date { get; set; }
    }
}
