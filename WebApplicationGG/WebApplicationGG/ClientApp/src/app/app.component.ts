import { Component, NgZone} from '@angular/core';
import { Message } from '../models/Message';
import { Button } from '../models/Button';
import { MessageService } from '../services/message.service';
import { ButtonService } from '../services/button.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'ClientApp';
  btnColour = 'red';
  inputText: string = '';
  uniqueID: string = new Date().getMilliseconds().toString();
  messages = new Array<Message>();
  message = new Message();
  button = new Button();

  constructor(
    private messageService: MessageService,
    private buttonService: ButtonService,
    private _ngZone: NgZone
  ) {
    this.subscribeToMessagesEvents();
     this.subscribeToButtonsEvents();
  }

  changeBtnColour() {
    this.btnColour = this.btnColour == 'blue' ? 'red' : 'blue';
    this.button = new Button();
    this.button.clientUniqueId = this.uniqueID;
    this.button.colour = this.btnColour;
    this.buttonService.changeColour(this.button);
  }

  sendText(): void {
    if (this.inputText) {
      this.message = new Message();
      this.message.clientUniqueId = this.uniqueID;
      this.message.message = this.inputText;
      this.message.date = new Date();
      this.messages.push(this.message);
      this.messageService.sendMessage(this.message);
      console.log('Send Message...' + this.inputText);
    }
  }
  private subscribeToMessagesEvents(): void {
    this.messageService.messageReceived.subscribe((message: Message) => {
      this._ngZone.run(() => {
        if (message.clientUniqueId !== this.uniqueID) {
          this.messages.push(message);
          this.inputText = message.message;
          console.log('Receive Message...' + this.inputText);
        }
      });
    });
  }

  private subscribeToButtonsEvents(): void {
    this.buttonService.colourChanged.subscribe((button: Button) => {
      this._ngZone.run(() => {
        if (button.clientUniqueId !== this.uniqueID) {
          this.btnColour = button.colour;
          console.log('Colour changed...');
        }
      });
    });
  }
}  
