import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Button } from '../models/Button';

@Injectable()
export class ButtonService {
  colourChanged = new EventEmitter<Button>();
  connectionEstablished = new EventEmitter<Boolean>();
  private hubConnection: HubConnection;

  constructor() {
    this.createConnection();
    this.registerOnServerEvents();
    this.startConnection();
  }

  changeColour(button: Button) {
    this.hubConnection.invoke('ChangeColourButton', button);
  }

  private createConnection() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(window.location.href + 'ButtonHub')
      .build();
  }

  private startConnection(): void {
    this.hubConnection
      .start()
      .then(() => {
        console.log('Hub connection started');
        this.connectionEstablished.emit(true);
      })
      .catch(err => {
        console.log('Error while establishing connection, retrying...', err);
        setTimeout(function () { this.startConnection(); }, 5000);
      });
  }

  private registerOnServerEvents(): void {
    this.hubConnection.on('ColourButtonChanged', (data: any) => {
      this.colourChanged.emit(data);
    });
  }
}  
