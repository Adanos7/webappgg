﻿using System.Threading.Tasks;
using WebApplicationGG.Models;
using Microsoft.AspNetCore.SignalR;

namespace WebApplicationGG.Hubs
{
    public class ButtonHub: Hub
    {
        public async Task ChangeColourButton(Button btn)
        {
            await Clients.All.SendAsync("ColourButtonChanged", btn);
        }
    }
}
